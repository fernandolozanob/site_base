<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Ecole_voiture">
    <meta name="author" content="Fernando Lozano ">
    <link rel="icon" href="../../favicon.ico">
 

    <title>
      
    </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Auto-ecole</a>

        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
            <li><a href="incription.php">Incription</a></li>
            <li><a href="contacte.php">Contate</a></li>
            <li><a href="panel.php">Espace eleve</a></li>
             <li><a href="panelTutor.php">Espace Tutor</a></li>
          </ul>
          

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">voitures/tarif</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/num1.jpeg" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>chevrolet</h4>
              <span class="text-muted">Modele 2014</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/imgc1.jpeg" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Mazda</h4>
              <span class="text-muted">Modele 2013</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/imgm2.jpeg" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Mazda</h4>
              <span class="text-muted">Modele 2015</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/imgc2.jpeg" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Ferrari</h4>
              <span class="text-muted">Modele 2011</span>
            </div>
          </div>

          <h2 class="sub-header">Tarif</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Voiture</th>
                  <th>Modele</th>
                  <th>Leçon</th>
                  <th>Offre</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Mazda</td>
                  <td>2010</td>
                  <td>10</td>
                  <td>5%</td>
                  <td>800€</td>
                </tr>
                      
                <tr>
                  <td>Mazda</td>
                  <td>2005</td>
                  <td>12</td>
                  <td>10%</td>
                  <td>1000€</td>
                </tr>
                  <tr>
                  <td>Chevrolet</td>
                  <td>2008</td>
                  <td>14</td>
                  <td>10%</td>
                  <td>1200€</td>
                </tr>
                  <tr>
                  <td>Crevrolet</td>
                  <td>2009</td>
                  <td>16</td>
                  <td>12%</td>
                  <td>130€</td>
                </tr>
                  <tr>
                  <td>Mazda</td>
                  <td>2010</td>
                  <td>18</td>
                  <td>12%</td>
                  <td>1400€</td>
                </tr>
                  <tr>
                  <td>Ferrari</td>
                  <td>2022</td>
                  <td>20</td>
                  <td>15%</td>
                  <td>1800€</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
